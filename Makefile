# Usage
# make			# compile document
# make clean	# remove working files
# make test		# observe commands with all the variables inserted
# make draft	# just string together the files
# make final	# just convert the strung-together PDF into an ebook version

# give the output a name

FILE    = simplex-relay-operator-package

# give the PDF type setting for Ghostscript
# choices: ebook, printer, prepress

DETAIL	= ebook

# define the order

ORDER	= J1 K1 L1 B2 B1 I1 A2 A4 A5 A6 A7 E1 H1 C3

# define the pdf modules 
#**Remember to put a \ in front of each space if outside quotes**

SOURCEA   = Rules\ and\ Guide\ master.pdf
SOURCEB   = VECTOR\ Simplex\ Relay\ Day\ Score\ Sheet\ Alternative\ C\ master.docx.pdf
SOURCEC   = Credits\ master.pdf
SOURCED   = VECTOR\ DAY.pdf 
SOURCEE   = PERCS_Message_Form_Ver1.4.pdf
SOURCEF   = Radiogram-VA3IDJ-colour.pdf
SOURCEG   = RADIOGRAM-2011.pdf
SOURCEH	  = Useful\ links\ master.pdf
SOURCEI   = VECTOR\ -\ Simplex\ Relay\ Day\ 2020\ -\ Flow\ Diagram.pdf
SOURCEJ   = VECTOR\ -\ Simplex\ Relay\ Day\ 2020\ Cover.pdf
SOURCEK   = VECTOR\ Simplex\ Relay\ Day\ Overview\ 2020.pdf
SOURCEL   = Rules\ and\ Guide\ alternative\ with\ VECTOR\ channels.pdf

all: pdf 

pdf: final named

.PHONY = all clean test draft pdf

# Shrink the package (Final Target)

final: $(FILE).pdf $(FILE)-$(DETAIL).pdf

$(FILE)-$(DETAIL).pdf: $(FILE).pdf 
	@echo "creating $(DETAIL)";
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.5 -dPDFSETTINGS=/$(DETAIL) -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(FILE)-$(DETAIL).pdf $(FILE).pdf

# Assemble the package (subtarget)

draft: test1.pdf $(SOURCEA) $(SOURCEB) $(SOURCEC) $(SOURCED) $(SOURCEE) $(SOURCEF) $(SOURCEG) $(SOURCEH) $(SOURCEI) $(SOURCEJ) $(SOURCEK) $(SOURCEL)

test1.pdf: $(SOURCEA) $(SOURCEB) $(SOURCEC) $(SOURCED) $(SOURCEE) $(SOURCEF) $(SOURCEG) $(SOURCEH) $(SOURCEI) $(SOURCEJ) $(SOURCEK) $(SOURCEL)
	@echo "assembling...";
	pdftk A=$(SOURCEA) B=$(SOURCEB) C=$(SOURCEC) D=$(SOURCED) E=$(SOURCEE) F=$(SOURCEF) G=$(SOURCEG) H=$(SOURCEH) I=$(SOURCEI) J=$(SOURCEJ) K=$(SOURCEK) L=$(SOURCEL) cat $(ORDER) output test1.pdf



# Name the package to short name (subtarget)

named: $(FILE).pdf test1.pdf 

$(FILE).pdf: test1.pdf
	cp test1.pdf $(FILE).pdf

# Check the varible expansions

test:
	@echo "pdftk A=$(SOURCEA) B=$(SOURCEB) C=$(SOURCEC) D=$(SOURCED) E=$(SOURCEE) F=$(SOURCEF) G=$(SOURCEG) H=$(SOURCEH) I=$(SOURCEI) J=$(SOURCEJ) K=$(SOURCEK) L=$(SOURCEL) cat $(ORDER) output test1.pdf";
	@echo "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.5 -dPDFSETTINGS=$(DETAIL) -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(FILE)-ebook.pdf $(FILE).pdf";
	@echo "cp test1.pdf $(FILE).pdf"

# Clean up the working files

clean:
	rm -f test1.pdf
	rm -f $(FILE)*.pdf