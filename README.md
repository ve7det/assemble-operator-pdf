# Assembling the Relay Operator Package

For the [VECTOR Simplex Relay Day](https://vectorradio.ca/simplex-relay-day), we assembled the modular files in Google Drive, then one of the team downloaded PDF's of the files.  Using PDFTK, we compiled these into one file for people to use.  

We created "master" files and iteratively "mixed" updates into the files and "remixed" each file.  At major points in our work, we replaced the old "master" with a new one.  

## Table of Contents

* [Usage](#usage)
* [Installation](#installation)
* [Uses](#uses)
* [Needs](#needs)
* [Contributing](#contributing)
* [Credits](#credits)
* [License](#license)
* [Donations](#donations)

## Usage

This project follows the model set by Pandoc-Resume and Pandoc-Letter without yet using any Pandoc tools. 

1. Review and revise the options in the Makefile variables
    - output name
    - PDF output type (ebook, printer, prepress)
    - file order and pages (PDFTK syntax)
    - names of the PDF's to string together

2. Run make

```shell
$ make
```


## Installation

1. Download the repository into a working folder where you collected the PDF's 
2. Extract the files into the folder with your PDF's

## Uses

* PDFTK
* Ghostscript
* Automake

## Needs

Needs (in the same directory) the named PDF files exported from Google Drive. These make up the modules that this method strings together into the operator package.  

* Rules and Guide master.pdf
* VECTOR Simplex Relay Day Score Sheet Alternative C master.docx.pdf
* Credits master.pdf
* VECTOR DAY.pdf
* PERCS_Message_Form_Ver1.4.pdf
* Radiogram-VA3IDJ-colour.pdf
* RADIOGRAM-2011.pdf
* Useful links master.pdf
* VECTOR - Simplex Relay Day 2020 - Flow Diagram.pdf
* VECTOR - Simplex Relay Day 2020 Cover.pdf
* VECTOR Simplex Relay Day Overview 2020.pdf
* Rules and Guide alternative with VECTOR channels.pdf

## Contributing

Welcome to, pretty informal.  

## Credits

* Jesse VE7DET
* Nick VE7NTX

## License

Looking at Creative Commons Share and Share Alike.  

## Donations

We accept donations at the [VECTOR website](https://vectorradio.ca/donate/)